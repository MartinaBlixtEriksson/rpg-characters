package equipment;


public class Weapon extends Equipment{

    private double damage;
    private double attackSpeed;
    private double dps;
    private WeaponType weaponType;

    public double getDps() {
        return dps;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public Weapon(String name, int levelRequired, Slot slot, double damage, double attackSpeed, WeaponType weaponType) {
        super(name, levelRequired, slot);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
        this.dps = damage * attackSpeed;
    }
}
