package equipment;

import characters.PrimaryAttribute;

public class Armor extends Equipment {
    private ArmorType armorType;
    private PrimaryAttribute stats;

    public PrimaryAttribute getStats() {
        return stats;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public Armor(String name, int levelRequired, Slot slot, ArmorType armorType, int strength, int intelligence, int dexterity) {
        super(name, levelRequired, slot);
        this.stats = new PrimaryAttribute(strength, dexterity, intelligence);
        this.armorType = armorType;
        this.slot = slot;
    }
}
