package equipment;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
