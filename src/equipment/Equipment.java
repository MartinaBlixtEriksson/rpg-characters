package equipment;

public abstract class Equipment {
    private String name;
    private int levelRequired;
    protected Slot slot;

    public Equipment(String name, int levelRequired, Slot slot) {
        this.name = name;
        this.levelRequired = levelRequired;
        this.slot = slot;
    }

    public Slot getSlot() {
        return slot;
    }

    public String getName() {
        return name;
    }


    public int getLevelRequired() {
        return levelRequired;
    }


}
