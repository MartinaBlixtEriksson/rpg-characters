package characters;

import equipment.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Character implements PlayableCharacter{

    public Character(String name) {
        this.name = name;
    }

    private String name;
    private int level = 1;

    protected double characterDps;
    protected Map<Slot, Equipment> equipment;
    protected PrimaryAttribute baseStats;
    private PrimaryAttribute totalStats;
    protected List<ArmorType> allowedArmorType = new ArrayList<>();
    protected List<WeaponType> allowedWeaponType = new ArrayList<>();


    public String getName() {
        return name;
    }
    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public Map getEquipment() {
        return equipment;
    }
    public PrimaryAttribute getBaseStats() {
        return baseStats;
    }
    public void setBaseStats(PrimaryAttribute baseStats) {
        this.baseStats = baseStats;
    }
    public PrimaryAttribute getTotalStats() {
        return totalStats;
    }
    public void setTotalStats(PrimaryAttribute totalStats) {
        this.totalStats = totalStats;
    }

    // protected abstract method
    protected abstract double calculateTotalMainAttribute();

    // save the base stats to array
    public int[] getAttributes(){
        return new int[]{this.baseStats.getStrength(), this.baseStats.getDexterity(), this.baseStats.getIntelligence()};
    }
    @Override
    public String displayCharacterStats(){
        StringBuilder builder = new StringBuilder();
        builder.append("Name: "+ this.name);
        builder.append("\nLevel: " + this.level);
        builder.append("\nStrength: " + this.getTotalStats().getStrength());
        builder.append("\nDexterity: " + this.getTotalStats().getDexterity());
        builder.append("\nIntelligence: " + this.getTotalStats().getIntelligence());
        builder.append("\nDPS: " + this.calculateCharacterDps());
        return builder.toString();
    }

    // method for equipping weapon
    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // Throw exception if user tries to equip wrong weapon type.
        String weaponTypeLowercase = weapon.getWeaponType().toString().toLowerCase();
        if(!allowedWeaponType.contains(weapon.getWeaponType())){
            throw new InvalidWeaponException("You cannot equip " + weaponTypeLowercase + ". Wrong type.");
        }
        // Throw exception if item level is too high.
        if(weapon.getLevelRequired() > this.getLevel()){
            throw new InvalidWeaponException("Item level too high. Required level: " + weapon.getLevelRequired() + " Your level: " + this.getLevel());
        }
        equipment.put(weapon.getSlot(), weapon);
        return true;
    }

    // method for equipping armor
    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        // If the armor type is not of the right type, throw exception
        if(!allowedArmorType.contains(armor.getArmorType())){
            String armorTypeLowercase = armor.getArmorType().toString().toLowerCase();
            throw new InvalidArmorException("You cannot equip " + armorTypeLowercase + " armor.");
        }
        // If item level is too high, throw exception
        if(armor.getLevelRequired() > this.getLevel()){
            throw new InvalidArmorException("Item level too high. Required level: " + armor.getLevelRequired() + " Your level: " + this.getLevel());
        }
        equipment.put(armor.getSlot(), armor);
        return true;
    }

    // Calculate the total PrimaryAttributes of the character object
    @Override
    public void calculateTotalStats() {
        int strength = 0;
        int intelligence = 0;
        int dexterity = 0;

        // get stats from equipment
        if (this.equipment != null){
            for (Equipment value : equipment.values()){
                if (value instanceof Armor armor){
                    strength += armor.getStats().getStrength();
                    intelligence += armor.getStats().getIntelligence();
                    dexterity += armor.getStats().getDexterity();
                }
            }
        }
        // add stats from character
        strength += this.getBaseStats().getStrength();
        intelligence += this.getBaseStats().getIntelligence();
        dexterity += this.getBaseStats().getDexterity();

        // create new PrimaryAttribute
        PrimaryAttribute stats = new PrimaryAttribute(strength, dexterity, intelligence);

        // set total stats
        this.setTotalStats(stats);
    }

    // calculate character dps
    @Override
    public double calculateCharacterDps() {
        double weaponDps = 0d;
        double modifier = this.calculateTotalMainAttribute()*0.01 + 1;
        // if character has no weapon equipped, set weaponDps to 1.
        if(equipment.get(Slot.WEAPON) == null){
            System.out.println("weapon null");
            weaponDps = 1;
        }
        // get weapon dps
        else if(equipment.get(Slot.WEAPON) instanceof Weapon weapon){
            weaponDps =  weapon.getDps();
        }

        this.characterDps = weaponDps * modifier;
        return this.characterDps;
    }

}
