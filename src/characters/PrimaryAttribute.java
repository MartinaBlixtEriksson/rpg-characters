package characters;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public int getStrength() {
        return strength;
    }


    public int getDexterity() {
        return dexterity;
    }


    public int getIntelligence() {
        return intelligence;
    }

    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }
}
