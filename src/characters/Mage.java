package characters;

import equipment.*;

import java.util.Collections;
import java.util.HashMap;

public class Mage extends Character{

    // constructor
    public Mage(String name) {
        // name from the parent class constructor
        super(name);
        this.equipment = new HashMap<>();
        this.baseStats = new PrimaryAttribute(1, 1, 8);
        this.allowedArmorType.add(ArmorType.CLOTH);
        Collections.addAll(allowedWeaponType, WeaponType.STAFF, WeaponType.WAND);

    }

    // calculate the total of the characters main attribute
    @Override
    protected double calculateTotalMainAttribute(){
        calculateTotalStats();
        double totalMainAttribute = getTotalStats().getStrength();
        return totalMainAttribute;
    }

    // method for leveling up.
    @Override
    public void levelUp() {
        System.out.println("Ding! " + this.getName() + " leveled up.");
        PrimaryAttribute stats = new PrimaryAttribute(this.getBaseStats().getStrength()+1,this.getBaseStats().getDexterity()+1, this.getBaseStats().getIntelligence()+5);
        // set the new stats
        this.setBaseStats(stats);
        // increase level with 1.
        this.setLevel(this.getLevel()+1);
    }

}
