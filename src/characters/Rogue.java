package characters;

import equipment.*;

import java.util.Collections;
import java.util.HashMap;

public class Rogue extends Character{

    // constructor
    public Rogue(String name) {
        // name from the parent class constructor
        super(name);
        this.equipment = new HashMap<>();
        this.baseStats = new PrimaryAttribute(2, 6, 1);
        Collections.addAll(allowedArmorType, ArmorType.LEATHER, ArmorType.MAIL);
        Collections.addAll(allowedWeaponType, WeaponType.DAGGER, WeaponType.SWORD);

    }

    // calculate the total of the characters main attribute
    @Override
    protected double calculateTotalMainAttribute(){
        calculateTotalStats();
        double totalMainAttribute = getTotalStats().getStrength();
        return totalMainAttribute;
    }

    // method for leveling up.
    @Override
    public void levelUp() {
        System.out.println("Ding! " + this.getName() + " leveled up.");
        PrimaryAttribute stats = new PrimaryAttribute(this.getBaseStats().getStrength()+1,this.getBaseStats().getDexterity()+4, this.getBaseStats().getIntelligence()+1);
        // set the new stats
        this.setBaseStats(stats);
        // increase level with 1.
        this.setLevel(this.getLevel()+1);
    }

}
