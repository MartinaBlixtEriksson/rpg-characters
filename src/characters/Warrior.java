package characters;

import equipment.*;

import java.util.Collections;
import java.util.HashMap;

public class Warrior extends Character{

    // constructor
    public Warrior(String name) {
        // name from the parent class constructor
        super(name);
        this.equipment = new HashMap<>();
        this.baseStats = new PrimaryAttribute(5, 2, 1);
        Collections.addAll(allowedArmorType, ArmorType.MAIL, ArmorType.PLATE);
        Collections.addAll(allowedWeaponType, WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD);

    }

    // calculate the total of the characters main attribute
    @Override
    protected double calculateTotalMainAttribute(){
        calculateTotalStats();
        double totalMainAttribute = getTotalStats().getStrength();
        return totalMainAttribute;
    }

    // method for leveling up.
    @Override
    public void levelUp() {
        System.out.println("Ding! " + this.getName() + " leveled up.");
        PrimaryAttribute stats = new PrimaryAttribute(this.getBaseStats().getStrength()+3,this.getBaseStats().getDexterity()+2, this.getBaseStats().getIntelligence()+1);
        // set the new stats
        this.setBaseStats(stats);
        // increase level with 1.
        this.setLevel(this.getLevel()+1);
    }

}
