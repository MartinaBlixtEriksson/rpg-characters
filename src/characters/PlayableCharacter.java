package characters;

import equipment.Armor;
import equipment.InvalidArmorException;
import equipment.InvalidWeaponException;
import equipment.Weapon;

public interface PlayableCharacter {

    void levelUp();
    String displayCharacterStats();
    void calculateTotalStats();

    double calculateCharacterDps();
    boolean equipArmor(Armor armor) throws InvalidArmorException;
    boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;
}
