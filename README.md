# RPG Characters


## Description
A small console app made in Java. Here you can create a character of different classes. Characters have three attributes; strength, dexterity and intelligence. Each created character get these attributes in various amounts depending on the class. The attributes increases when the character levels up. Characters can equip different types of armor and weapons. The equipment alters the characters DPS.

Unit tests are provided in the test directory.


## Installation
clone project
```
git clone https://gitlab.com/MartinaBlixtEriksson/rpg-characters.git
```
Add jUnit5 to project

Run Main class
or 
Run tests

## Usage
Example of creating a character and creating and equipping items:
```
// Create a new mage
Mage mage = new Mage("Ourania");
// Create a weapon
Weapon staff = new Weapon("Ebonchill",1, Slot.WEAPON,20,2, WeaponType.STAFF);
// Create head armor
Armor head = new Armor("Veil of the Banshee Queen",1,Slot.HEAD, ArmorType.CLOTH,2,10,5);
// Equip
mage.equipArmor(head);
mage.equipWeapon(staff);
```


## Authors
[Martina Blixt Eriksson](@MartinaBlixtEriksson)
