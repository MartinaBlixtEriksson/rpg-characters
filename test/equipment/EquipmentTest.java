package equipment;

import characters.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EquipmentTest {

    @Test
    public void equipWeapon_weaponTooHighLevel_shouldThrowInvalidWeaponException(){
        // Arrange
        Warrior testWarrior = new Warrior("Lixiss");
        Weapon testWeapon = new Weapon("Common Axe", 2, Slot.WEAPON,7,1.1, WeaponType.AXE);
        String expected = "Item level too high. Required level: " + testWeapon.getLevelRequired() + " Your level: " + testWarrior.getLevel();
        String actual = "";

        // act and assert
        // throws exception?
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipWeapon(testWeapon));

        actual = exception.getMessage();
        // do the strings match?
        assertEquals(expected, actual);
    }

    @Test
    public void equipArmor_armorTooHighLevel_shouldThrowInvalidArmorException(){
        Warrior testWarrior = new Warrior("Lixiss");
        Armor testPlateBody = new Armor("Common PLate Body Armor", 2, Slot.BODY, ArmorType.PLATE, 1,0,0);
        String expected = "Item level too high. Required level: " + testPlateBody.getLevelRequired() + " Your level: " + testWarrior.getLevel();
        String actual = "";

        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipArmor(testPlateBody));

        actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equipWeapon_weaponOfWrongType_shouldThrowInvalidWeaponException(){
        Warrior testWarrior = new Warrior("Lixiss");
        Weapon testBow = new Weapon("Common Bow", 1, Slot.WEAPON, 12, 0.8, WeaponType.BOW);
        String weaponTypeLowercase = testBow.getWeaponType().toString().toLowerCase();
        String expected = "You cannot equip " + weaponTypeLowercase + ". Wrong type.";
        String actual = "";

        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipWeapon(testBow));

        actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equipArmor_armorOfWrongType_shouldThrowInvalidArmorException(){
        Warrior testWarrior = new Warrior("Lixiss");
        Armor testClothHead = new Armor("Common Cloth Head Armor", 1, Slot.HEAD, ArmorType.CLOTH, 0,5, 0);
        String armorTypeLowercase = testClothHead.getArmorType().toString().toLowerCase();
        String expected = "You cannot equip " + armorTypeLowercase + " armor.";
        String actual = "";

        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipArmor(testClothHead));

        actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equipWeapon_weaponIsValid_returnTrue() throws InvalidWeaponException {
        Warrior testWarrior = new Warrior("Lixiss");
        Weapon testWeapon = new Weapon("Common Axe", 1, Slot.WEAPON,7,1.1, WeaponType.AXE);
        boolean expected = true;
        boolean actual;

        actual = testWarrior.equipWeapon(testWeapon);

        assertTrue(actual);
    }

    @Test
    public void equipArmor_armorIsValid_returnTrue() throws InvalidArmorException {
        Warrior testWarrior = new Warrior("Lixiss");
        Armor testPlateBody = new Armor("Common PLate Body Armor", 1, Slot.BODY, ArmorType.PLATE, 1,0,0);
        boolean expected = true;
        boolean actual;

        actual = testWarrior.equipArmor(testPlateBody);

        assertTrue(actual);
    }

    @Test
    public void calculateDps_noWeaponEquipped_dps1(){
        Warrior testWarrior = new Warrior("Lixiss");
        double expected =  1*(1 + (5.0 / 100));
        double actual = testWarrior.calculateCharacterDps();

        assertEquals(expected,actual);

    }

    @Test
    public void calculateDps_weaponEquipped_dps() throws InvalidWeaponException {
        Warrior testWarrior = new Warrior("Lixiss");
        Weapon testWeapon = new Weapon("Common Axe", 1, Slot.WEAPON,7,1.1, WeaponType.AXE);
        double expected =  (7 * 1.1)*(1 + (5.0 / 100));
        double actual;
        testWarrior.equipWeapon(testWeapon);
        actual = testWarrior.calculateCharacterDps();

        assertEquals(expected, actual);
    }

    @Test
    public void calculateDps_weaponAndArmorEquipped_dps7() throws InvalidWeaponException, InvalidArmorException {
        Warrior testWarrior = new Warrior("Lixiss");
        Armor testPlateBody = new Armor("Common PLate Body Armor", 1, Slot.BODY, ArmorType.PLATE, 1,0,0);
        Weapon testWeapon = new Weapon("Common Axe", 1, Slot.WEAPON,7,1.1, WeaponType.AXE);
        double expected = (7 * 1.1) * (1+((5.0+1) /100));
        double actual;

        testWarrior.equipWeapon(testWeapon);
        testWarrior.equipArmor(testPlateBody);

        actual = testWarrior.calculateCharacterDps();

        assertEquals(expected, actual);

    }





}
