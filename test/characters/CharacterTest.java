package characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    // I made one of each class, realized after that wasn't necessary according to the specs.
    @Test
    public void createWarrior_isCorrectLevel_level1(){
        Warrior testWarrior = new Warrior("Lixiss");
        int expected = 1;
        int actual;

        actual = testWarrior.getLevel();

        assertEquals(expected, actual);
    }
    @Test
    public void createRanger_isCorrectLevel_level1(){
        Ranger testRanger = new Ranger("Flomph");
        int expected = 1;
        int actual;

        actual = testRanger.getLevel();

        assertEquals(expected, actual);
    }
    @Test
    public void createRogue_isCorrectLevel_level1(){
        Rogue testRogue = new Rogue("SneakyGalosh");
        int expected = 1;
        int actual;

        actual = testRogue.getLevel();

        assertEquals(expected, actual);
    }
    @Test
    public void createMage_isCorrectLevel_level1(){
        Mage testMage = new Mage("Ourania");
        int expected = 1;
        int actual;

        actual = testMage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    public void levelUpCharacter_isCorrectLevel_level2(){
        Mage testMage = new Mage("Ourania");
        int expected = 2;
        int actual;

        testMage.levelUp();
        actual = testMage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    public void createMage_characterHasCorrectStats_correctPrimaryAttributes(){
        Mage testMage = new Mage("Ourania");

        int[] expected = {1, 1, 8};
        int[] actual = testMage.getAttributes();
        assertArrayEquals(expected,actual);
    }

    @Test
    public void createRanger_characterHasCorrectStats_correctPrimaryAttributes(){
        Ranger testRanger = new Ranger("Flomph");

        int[] expected = {1, 7, 1};
        int[] actual = testRanger.getAttributes();
        assertArrayEquals(expected,actual);
    }

    @Test
    public void createRogue_characterHasCorrectStats_correctPrimaryAttributes(){
        Rogue testRogue  = new Rogue("SneakyGalosh");

        int[] expected = {2, 6, 1};
        int[] actual = testRogue.getAttributes();
        assertArrayEquals(expected,actual);
    }
    @Test
    public void createWarrior_characterHasCorrectStats_correctPrimaryAttributes(){
        Warrior testWarrior = new Warrior("Lixiss");

        int[] expected = {5, 2, 1};
        int[] actual = testWarrior.getAttributes();
        assertArrayEquals(expected,actual);
    }

    @Test
    public void levelUpMage_characterGainsCorrectStats_correctPrimaryAttributes(){
        Mage testMage = new Mage("Ourania");

        int[] expected = {2, 2, 13};
        int[] actual;

        testMage.levelUp();

        actual = testMage.getAttributes();
        assertArrayEquals(expected, actual);
    }
    @Test
    public void levelUpRanger_characterGainsCorrectStats_correctPrimaryAttributes(){
        Ranger testRanger = new Ranger("Flomph");

        int[] expected = {2, 12, 2};
        int[] actual;

        testRanger.levelUp();

        actual = testRanger.getAttributes();
        assertArrayEquals(expected, actual);
    }
    @Test
    public void levelUpRogue_characterGainsCorrectStats_correctPrimaryAttributes(){
        Rogue testRogue  = new Rogue("SneakyGalosh");

        int[] expected = {3, 10, 2};
        int[] actual;

        testRogue.levelUp();

        actual = testRogue.getAttributes();
        assertArrayEquals(expected, actual);
    }
    @Test
    public void levelUpWarrior_characterGainsCorrectStats_correctPrimaryAttributes(){
        Warrior testWarrior = new Warrior("Lixiss");

        int[] expected = {8, 4, 2};
        int[] actual;

        testWarrior.levelUp();

        actual = testWarrior.getAttributes();
        assertArrayEquals(expected, actual);
    }




}